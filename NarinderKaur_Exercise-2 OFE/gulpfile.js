var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function(done) {
    gulp.src('./app/main/assets/scss/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./app/main/assets/css/'));
});

var browserSync = require('browser-sync').create();

gulp.task('browse', function() {
  browserSync.init({
    server: {
      baseDir: 'app/main'
    },
  })
})
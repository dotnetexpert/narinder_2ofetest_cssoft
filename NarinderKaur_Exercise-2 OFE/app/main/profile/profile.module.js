var app = angular
        .module('app.profile', ['ngMaterial','ui.router','profile.components','profile.services']);
        
   app.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {

	$stateProvider
		.state('profile', {
			url: '/profile',
            templateUrl: '../profile/components/profile/profile.html',
            controller : 'ProfileController as vm',
            bodyClass: 'profile'
		});

	$urlRouterProvider.otherwise('/profile');
}]);

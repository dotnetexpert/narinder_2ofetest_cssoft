(function ()
{
  'use strict';

 angular.module('profile.components').controller('AddUserDialogController', AddUserDialogController);

  /** @ngInject */
  function AddUserDialogController($mdDialog,$scope)
  {
    var vm = this;

    // Methods
    vm.closeDialog = closeDialog;
    vm.addUser = addUser;
    //////////

    function closeDialog()
    {
      $mdDialog.hide();
    };
    
    function addUser()
    {
     
      if($scope.userform.$invalid==false)
      {
        $scope.userform.account.isAdd=true;
        $mdDialog.hide($scope.userform.account);
      }
    };
  }
  
  //  angular.module('profile.components').component('zm-add-user', {
  //     templateUrl: '../profile/components/dialogs/add-user/add-user-detail.html',
  //     controller: 'AddUserDialogController as vm'
  //  });
})();

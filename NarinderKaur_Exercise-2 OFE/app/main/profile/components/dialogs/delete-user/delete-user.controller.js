(function ()
{
  'use strict';

 angular.module('profile.components').controller('DeleteUserDialogController', DeleteUserDialogController);

  /** @ngInject */
  function DeleteUserDialogController($mdDialog)
  {
    var vm = this;

    // Methods
    vm.closeDialog = closeDialog;
    vm.deleteUser = deleteUser;

    //////////

    function closeDialog()
    {
      $mdDialog.hide();
    };
    
    function deleteUser()
    {
      $mdDialog.hide(true);
    };
  }
  
})();

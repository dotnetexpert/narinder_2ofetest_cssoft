(function ()
{
    'use strict';

    /** @ngInject */
    function ProfileController()
    {
        var vm = this;
        var klick = false;
    }

    angular.module('profile.components').controller('ProfileController', ProfileController);
    
      angular.module('profile.components').component('profile', {
      templateUrl: '../profile/components/profile/profile-detail.html',
      controller: 'ProfileController as vm'
       });
})();

(function ()
{
    'use strict';

    /** @ngInject */
    function UserController($scope,$mdDialog, $document,zmUserProfileService,$mdToast)
    {
        var vm = this;
        var klick = false;
        vm.selected = false;
         
      /**
       * Open Detail View of an List entry
       */
      function openDetailView(entry)
      {
        vm.selected = entry;
      }
      
      //   // Data
      vm.getContacts =function() {
            zmUserProfileService.ReadListOfUser().then(
               function(users) {
                vm.contacts=users.data;
               }
             );
      };
      vm.getContacts();

      vm.dtOptions = {
        dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
        pagingType: 'simple',
        autoWidth : false,
        responsive: true
      };

      // Methods
      vm.openDetailView= openDetailView;
      vm.AddUserDialog= AddUserDialog;
      vm.DeleteUserDialog= DeleteUserDialog;
      vm.UpdateUserDetail= UpdateUserDetail;
 
      /**
       * Open add new contact dialog
       *
       * @param ev
       */
      function AddUserDialog(ev)
      {
        $mdDialog.show({
          controller         : 'AddUserDialogController',
          controllerAs       : 'vm',
          locals             : {
            selectedMail: undefined
          },
          templateUrl        : '../profile/components/dialogs/add-user/add-user.html',
          parent             : angular.element($document[0].body),
          targetEvent        : ev,
          clickOutsideToClose: false
         
        }).then(function(userData) {
            if(userData!=undefined && userData.isAdd==true)
          {
              var newUser={
                     "username": userData.firstName,
                     "userid": vm.contacts.length + 1,
                     "company": "Zippel Media",
                     "job": "Developer",
                     "location": "Elsdorf", "province": "North-Rhine-Westfalia",
                     "avatar": "assets/avatars/userdefault.png",
                     "type": "user"
             };
             vm.contacts.splice(0,0,newUser);
             vm.selected=true;
             vm.selected=newUser;
             $mdToast.show(
                    $mdToast.simple()
                    .content("User added successfully...")
					.theme('success-toast')
					.position('top right')
             );
           
          }
            }, function(userData) {
               
            });
      }
      
       /**
       * update user detail
       *
       * @param ev
       */
      function UpdateUserDetail(selectedUser)
      {       
          console.log(selectedUser);
      }
      
      /**
     * Open delete user dialog
     *
     * @param ev
     */
    function DeleteUserDialog(ev,selectedUser)
    {
      $mdDialog.show({
        controller         : 'DeleteUserDialogController',
        controllerAs       : 'vm',
        locals             : {
          selectedMail: undefined
        },
        templateUrl        : '../profile/components/dialogs/delete-user/delete-user.html',
        parent             : angular.element($document.body),
        targetEvent        : ev,
        clickOutsideToClose: true
      }).then(function(isDelete) {
          if(isDelete==true)
          {
          vm.contacts.splice(selectedUser,1);
          vm.selected=false;
             $mdToast.show(
                    $mdToast.simple()
                    .content("User deleted successfully...")
					.theme('success-toast')
					.position('top right')
             );
          }
            }, function() {
               
            });
    }

    }

    angular.module('profile.components').controller('UserController', UserController);
    
      angular.module('profile.components').component('user', {
      templateUrl: '../profile/components/tabs/user/user-detail.html',
      controller: 'UserController as vm'
       });
})();
